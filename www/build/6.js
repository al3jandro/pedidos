webpackJsonp([6],{

/***/ 501:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(508);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_auth_service__ = __webpack_require__(306);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, toast, alert, authService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.alert = alert;
        this.authService = authService;
        this.users = {
            email: '', pass: ''
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.signin = function () {
        var _this = this;
        console.log(this.users);
        this.authService.signIn(this.users.email, this.users.pass)
            .then(function (auhtData) {
            _this.navCtrl.setRoot('MisTabsPage');
            _this.toast.create({
                message: "Bienvenido " + _this.authService.get(),
                duration: 2000
            });
        })
            .catch(function (err) {
            console.log(err);
            _this.authService.setError(err.code);
        });
    };
    LoginPage.prototype.goToReset = function () {
        var _this = this;
        var alertMsg = this.alert.create({
            title: 'Reset Password',
            subTitle: 'Ingrese su Correo Electronico',
            inputs: [
                {
                    name: 'email',
                    placeholder: 'E-Mail',
                    type: 'email'
                }
            ],
            buttons: [
                {
                    text: 'Reset Password',
                    handler: function (data) {
                        _this.authService.forgoResetPassword(data.email)
                            .then(function () {
                            _this.toast.create({
                                message: 'La clave fue enviada al e-mail.',
                                duration: 3000,
                                position: 'bottom'
                            }).present();
                            alertMsg.dismiss();
                        })
                            .catch(function (err) {
                        });
                    }
                }
            ]
        });
        alertMsg.present();
    };
    LoginPage.prototype.goToAlta = function () {
        var _this = this;
        var alertMsg = this.alert.create({
            title: 'Registrar Usuario',
            inputs: [
                {
                    name: 'email',
                    placeholder: 'E-Mail',
                    type: 'email'
                },
                {
                    name: 'pass',
                    placeholder: 'Clave',
                    type: 'password'
                },
                {
                    name: 'nome',
                    placeholder: 'Nombre',
                    type: 'text'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Registrar!!',
                    handler: function (data) {
                        console.log(data);
                        _this.authService.signupInWithEmailAndPass(data.email, data.pass)
                            .then(function (res) {
                            console.log(res);
                            _this.authService.updateProfileAlta(data.nome);
                            _this.toast.create({
                                message: 'Bienvenido a la App.',
                                duration: 3000,
                                position: 'bottom'
                            }).present();
                            _this.navCtrl.setRoot('MisTabsPage').then(function () {
                                alertMsg.dismiss();
                            });
                        })
                            .catch(function (err) {
                            console.log(err);
                            _this.authService.setError(err.code);
                            alertMsg.dismiss();
                        });
                    }
                }
            ]
        });
        alertMsg.present();
    };
    LoginPage.prototype.signInWithTwitter = function () {
        var _this = this;
        this.authService.signInWithTwitter()
            .then(function () {
            _this.navCtrl.setRoot('MisTabsPage');
        })
            .catch(function (err) {
            console.log(err);
            _this.toast.create({
                duration: 3000,
                position: 'bottom',
                message: 'Error al entrar con Twitter'
            }).present();
        });
    };
    LoginPage.prototype.signInWithGoogle = function () {
        var _this = this;
        this.authService.signInWithGoogle()
            .then(function () {
            _this.navCtrl.setRoot('MisTabsPage');
        })
            .catch(function (err) {
            console.log(err);
            _this.toast.create({
                duration: 3000,
                position: 'bottom',
                message: 'Error al entrar con Google'
            }).present();
        });
    };
    LoginPage.prototype.signInWithFacebook = function () {
        var _this = this;
        this.authService.signInWithFacebook()
            .then(function () {
            _this.navCtrl.setRoot('MisTabsPage');
        })
            .catch(function (err) {
            console.log(err);
            _this.toast.create({
                duration: 3000,
                position: 'bottom',
                message: 'Error al entrar con Facebook'
            }).present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/hectorcanaimero/Documents/dev/daniel/2pedidos/src/app/modulos/seguridad/login/login.html"*/'<ion-content>\n  <div padding>\n    <img src="assets/imgs/logo-1.png" alt="">\n    <button ion-button \n      icon-left block \n      (click)="signInWithFacebook()"\n      color="facebook">\n      <ion-icon name="logo-facebook"></ion-icon>\n      Facebook\n    </button>\n    <button ion-button \n      icon-left block \n      (click)="signInWithGoogle()"\n      color="google">\n      <ion-icon name="logo-google"></ion-icon>\n      Google\n    </button>\n    <button ion-button \n      icon-left block \n      color="twitter" \n      (click)="signInWithTwitter()">\n      <ion-icon name="logo-twitter"></ion-icon>\n      Twitter\n    </button>\n    <ion-list inset>\n      <ion-item>\n        <ion-avatar item-left>\n          <img src="assets/imgs/person.png" />\n        </ion-avatar>\n        <ion-input type="email" [(ngModel)]="users.email" placeholder="Usuario"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-left>\n          <img src="assets/imgs/person.png" />\n        </ion-avatar>\n        <ion-input type="password" [(ngModel)]="users.pass" placeholder="Clave"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>Recordar Credenciales</ion-label>\n        <ion-toggle checked=""></ion-toggle>\n      </ion-item>\n    </ion-list>\n  </div>\n  <div class="row">\n    <div class="col">\n      <button ion-button clear (click)="goToReset()">Olvide Clave</button>\n      <button ion-button clear (click)="goToAlta()">Registrese ahora</button>\n    </div>\n    <div class="col">\n      <button ion-button block (click)="signin()">Ingresar</button>  \n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/hectorcanaimero/Documents/dev/daniel/2pedidos/src/app/modulos/seguridad/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__shared_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__shared_auth_service__["a" /* AuthService */]) === "function" && _e || Object])
    ], LoginPage);
    return LoginPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=6.js.map