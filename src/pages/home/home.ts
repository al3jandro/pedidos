import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  user: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private authFb: AngularFireAuth  
  ) {
    this.user = {
      'display': this.authFb.auth.currentUser.displayName,
      'photo_url': this.authFb.auth.currentUser.photoURL
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  goTo(){
    this.navCtrl.setRoot('LoginPage');
  }

}
