import { AngularFireAuth } from 'angularfire2/auth';
import { Component } from '@angular/core';
import { Platform, NavController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any;
  pages: any = {title: '', component: ''};
  users: any = {
    display: '', email: '', photo: ''
  };

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public alert: AlertController,
    private authFb: AngularFireAuth
    
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.session();
    });
  }

  private session(){
    this.authFb.authState
    .subscribe(user=>{
      if(user){
        this.users = {
          display: user.displayName, 
          email: user.email, 
          photo: user.photoURL
        };
        this.rootPage = 'MisTabsPage';
      } else{
        this.rootPage = 'LoginPage';
      }
    })
  }

  public openPage(item){}

  logout(){
    let alertMsg = this.alert.create({
      title: 'Pedidos',
      subTitle: 'Para cerrar session presione el boton Salir',
      buttons:[
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salir Sistema!',
          handler: data => {
            this.authFb.auth.signOut();
          }
        }
      ]
    })
    alertMsg.present();
  }
}

