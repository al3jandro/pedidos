import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { AuthService } from './../../../shared/auth-service';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  users: any = {
    email: '', pass:''
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toast: ToastController,
    public alert: AlertController,
    private authService: AuthService
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  signin(){
    console.log(this.users);
    this.authService.signIn(this.users.email, this.users.pass)
    .then(auhtData =>{
      this.navCtrl.setRoot('MisTabsPage')
      this.toast.create({
        message: `Bienvenido ${this.authService.get()}`,
        duration: 2000
      })
    })
    .catch(err=>{
      console.log(err);
      this.authService.setError(err.code);
    })
  }
  
  
  goToReset(){
    let alertMsg = this.alert.create({
      title: 'Reset Password',
      subTitle: 'Ingrese su Correo Electronico',
      inputs: [
        {
          name: 'email',
          placeholder: 'E-Mail',
          type: 'email'
        }
      ],
      buttons:[
        {
          text: 'Reset Password',
          handler: data => {
            this.authService.forgoResetPassword(data.email)
            .then(()=>{
              this.toast.create({
                message: 'La clave fue enviada al e-mail.',
                duration: 3000,
                position: 'bottom'
              }).present();
              alertMsg.dismiss();
            })
            .catch(err=>{

            })
          }
        }
      ]
    })
    alertMsg.present();

  }


  goToAlta(){
    let alertMsg = this.alert.create({
      title: 'Registrar Usuario',
      inputs: [
        {
          name: 'email',
          placeholder: 'E-Mail',
          type: 'email'
        },
        {
          name: 'pass',
          placeholder: 'Clave',
          type: 'password'
        },
        {
          name: 'nome',
          placeholder: 'Nombre',
          type: 'text'
        }
      ],
      buttons:[
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Registrar!!',
          handler: data => {
            console.log(data);
            this.authService.signupInWithEmailAndPass(data.email, data.pass)
            .then(res=>{
              console.log(res);
              this.authService.updateProfileAlta(data.nome);
              this.toast.create({
                message: 'Bienvenido a la App.',
                duration: 3000,
                position: 'bottom'
              }).present();
              this.navCtrl.setRoot('MisTabsPage').then(()=>{
                alertMsg.dismiss();
              })
            })
            .catch(err=>{
              console.log(err);
              this.authService.setError(err.code);
              alertMsg.dismiss();
            })
          }
        }
      ]
    })
    alertMsg.present();
  }


  signInWithTwitter(){
    this.authService.signInWithTwitter()
    .then(()=>{
      this.navCtrl.setRoot('MisTabsPage');
    })
    .catch(err=>{
      console.log(err);
      this.toast.create({
        duration: 3000,
        position: 'bottom',
        message: 'Error al entrar con Twitter'
      }).present();
    })
  }


  signInWithGoogle(){
    this.authService.signInWithGoogle()
    .then(()=>{
      this.navCtrl.setRoot('MisTabsPage');
    })
    .catch(err=>{
      console.log(err);
      this.toast.create({
        duration: 3000,
        position: 'bottom',
        message: 'Error al entrar con Google'
      }).present();
    })
  }


  signInWithFacebook(){
    this.authService.signInWithFacebook()
    .then(()=>{
      this.navCtrl.setRoot('MisTabsPage');
    })
    .catch(err=>{
      console.log(err);
      this.toast.create({
        duration: 3000,
        position: 'bottom',
        message: 'Error al entrar con Facebook'
      }).present();
    })
  }
}