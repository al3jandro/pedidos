import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-ventana',
  templateUrl: 'ventana.html',
})

export class VentanaPage{

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ){ }

  
}