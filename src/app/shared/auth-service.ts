import { AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase'; 


@Injectable()
export class AuthService{

  constructor(
    private authFb: AngularFireAuth,
    public alertCtrl: AlertController
  ){ }

  signIn(email: string, pass: string){
    return this.authFb.auth.signInWithEmailAndPassword(email, pass);
  }

  updateProfileAlta(nome: string){
    this.authFb.auth.currentUser.updateProfile({
      displayName: nome,
      photoURL: ''
    })
  }

  signupInWithEmailAndPass(email: any, pass: any){
    return this.authFb.auth.createUserWithEmailAndPassword(email, pass);
  }

  forgoResetPassword(email:string){
    return this.authFb.auth.sendPasswordResetEmail(email);
  }


  signInWithTwitter(){
    return this.authFb.auth.signInWithPopup(
      new firebase.auth.TwitterAuthProvider()
    )
  }


  signInWithFacebook(){
    return this.authFb.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider()
    )
  }


  signInWithGoogle(){
    return this.authFb.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    )
  }

  setError(code:string){
    switch(code){
      case 'auth/argument-error':
        this.alert('Los campos no pueden estar vacio');
        break;
      case 'auth/wrong-password':
        this.alert('Clave invalida');
        break;  
      case 'auth/invalid-email':
        this.alert('Usuario no existe');
        break;
      case 'auth/email-already-in-use':
        this.alert('Usuario ya existe');
        break; 
      case 'auth/internal-error':
        this.alert('Usuario ya existe');
        break;
    }
      
  }
  private alert(message: string){
    this.alertCtrl.create({
      title: 'Error',
      subTitle: message,
      buttons: ['Ok']
    }).present();
  }

  public get(){
    this.authFb.auth.currentUser.displayName;
  }
}